#!/bin/sh
BACKUPDIR=$1
PKG=$2

if [ -z "$BACKUPDIR" -o -z "$PKG" ]; then
	echo "Usage: restore.sh <backup directory> <package name>"
	exit 1
fi

APK="$BACKUPDIR/$PKG.apk"
USER_TAR="$BACKUPDIR/user-$PKG.tar"
USER_DE_TAR="$BACKUPDIR/user_de-$PKG.tar"

set -ex

# Install app
adb install "$APK"

# Make sure the app closes and stays closed
adb shell pm disable "$PKG"
adb shell am force-stop "$PKG"
adb shell pm clear "$PKG"

# Restore files
adb shell -e none -T tar xf - <"$USER_TAR"
adb shell -e none -T tar xf - <"$USER_DE_TAR"

# Remove caches
adb shell rm -rf /data/user{,_de}/0/"$PKG"/{cache,code_cache} 

# Adapt to new UID
PKGUID=$(adb shell pm list packages -U "$PKG" | cut -d':' -f3)
adb shell chown -R "$PKGUID:$PKGUID" /data/user/0/"$PKG" /data/user_de/0/"$PKG"

# Restore SELinux contexts
adb shell restorecon -F -R /data/user/0/"$PKG"
adb shell restorecon -F -R /data/user_de/0/"$PKG"

# Reenable package
adb shell pm enable "$PKG"
