Android backup when you only have `adb root`
============================================

These are basic backup and restore scripts for Android apps including their private data. They are intended to be used with devices on which only ADB root is available.

Tested and used on a Fairphone 3 with /e/ OS for upgrading from Android 9 to 10. YMMV.

Unfortunately, apps that use the Android KeyStore in specific ways seemingly can't be restored with this method (e.g., Element).

Usage
-----
1. Set up USB debugging including adb access and root for adb.
1. Run `adb root` to get root access.
1. Run `backup.sh <backup directory>` to backup all apps and their data. This includes system apps, but I haven't tried restoring data for them and this might cause problems in case you update to a new major Android version.
1. Update or wipe device.
1. Set up USB debugging including adb access and root for adb.
1. Run `adb root` to get root access.
1. Run `restore.sh <backup directory> <package name>` for every app that you want to restore data for.
1. Make sure to grant essential permissions such as "storage" to the restored apps since permissions are not transferred and the apps will usually assume that they already have them (so they are not re-requested).
1. Optionally run `adb install <backup directory>/<package name>.apk` for every app that you want to have back but not restore data for.

**Warning: This does not back up user storage (internal, SD card etc.).**

Rationale
---------
* I could use some backup application available in the stores, but they all need root and
  * I can't boot TWRP since my device is locked.
  * I want to keep my device locked for security reasons.
  * I can't install SuperSU or Magisk in recovery since I can't boot TWRP and the default recovery only accepts correctly signed packages.
  * I can't install SuperSU or Magisk with adb since I can't mount /system read-write due to dm-verity.
* The non-root alternative `adb backup` does not work for banking apps, most messengers etc.

Disclaimer
----------
**You are running commands with superuser rights on your phone, so you can potentially brick your device. Always exercise due caution and keep multiple backups.**

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

If the disclaimer of warranty and limitation of liability provided above cannot be given local legal effect according to their terms, reviewing courts shall apply local law that most closely approximates an absolute waiver of all civil liability in connection with the Program, unless a warranty or assumption of liability accompanies a copy of the Program in return for a fee.
