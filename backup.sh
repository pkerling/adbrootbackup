#!/bin/sh
BACKUPDIR=$1
if [ -z "$BACKUPDIR" ]; then
	echo "Usage: backup.sh <backup directory>"
	exit 1
fi

echo "Backup directory: $BACKUPDIR"
mkdir -p "$BACKUPDIR"
for package in $(adb shell pm list packages | cut -d':' -f2); do
	echo -n "* $package: "
	APK=$(adb shell pm path "$package" | cut -d':' -f2)
	adb pull "$APK" "$BACKUPDIR/$package.apk" >/dev/null
	echo -n "apk "
	adb shell -e none -n -T tar cf - "data/user/0/$package" >"$BACKUPDIR/user-$package.tar"
	echo -n "user "
	adb shell -e none -n -T tar cf - "data/user_de/0/$package" >"$BACKUPDIR/user_de-$package.tar"
	echo "user_de"
done
